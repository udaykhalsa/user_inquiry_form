from django.contrib import admin
from .models import UserInquiry


class UserInquiryAdmin(admin.ModelAdmin):
 list_display = ['customer_name', 'email_id', 'phone_number']
 search_fields = ['customer_name', 'phone_number']

admin.site.register(UserInquiry, UserInquiryAdmin)
