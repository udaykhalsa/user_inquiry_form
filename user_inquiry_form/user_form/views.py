from django.shortcuts import redirect, render
from random import randint
from .models import UserInquiry
from twilio.rest import Client
from django.core.mail import send_mail

sid = 'AC29167be64aff1da1d0aca203ec1437bb'
token = 'ed49c72fae46040c94de6f4de3c25294'

client = Client(sid, token)


def generate_otp(number):
  global otp
  otp = ''.join(str(randint(0, 9)) for _ in range(6))
  print(otp)
  message = client.messages.create(
    body = f'This is an OTP {otp} for user inquiry form.',
    from_ = '+16075644784',
    to = f'+91{number}'
  )
  # with below code we can also verify email ID after creating user email for us1
  # send_mail(
    # 'OTP for User Inquiry Form',
    # f'This is an OTP {otp} for user inquiry form.',
    # 'from@example.com',
    # ['to@example.com'],
    # fail_silently=False,
# )


def get_data_view(request):
 if request.method == 'POST':
  model = UserInquiry
  name = request.POST.get('name')
  email_id = request.POST.get('email')
  phone_number= request.POST.get('phone_number')
  product_start_date= request.POST.get('start_date')
  product_end_date= request.POST.get('end_date')
  customer_message   = request.POST.get('description')

  model.objects.create(
    customer_name = name,
    email_id = email_id,
    phone_number = phone_number,
    product_start_date = product_start_date,
    product_end_date = product_end_date,
    customer_message = customer_message
  )
  
  context = {
    'name': name,
    'email_id': email_id,
    'phone_number': phone_number,
    'product_start_date': product_start_date,
    'product_end_date': product_end_date,
    'customer_message': customer_message
  }
  generate_otp(phone_number)
  return render(request, 'user_form/verify_otp.html', context)
 return render(request, 'user_form/index.html')


def confirm_otp(request):
 if request.method == 'POST':
   fetched_otp = request.POST.get('otp')

   if fetched_otp != otp:
     context = {
       'verified': False
     }
     return render(request, 'user_form/otp_verified.html', context)
   else:
    context = {
       'verified': True
     }
    return render(request, 'user_form/otp_verified.html', context)
 return render(request, 'user_form/verify_otp.html')