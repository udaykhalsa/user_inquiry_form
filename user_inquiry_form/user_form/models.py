from django.db import models

class UserInquiry(models.Model):
 customer_name = models.CharField(max_length=100)
 email_id = models.EmailField()
 phone_number = models.IntegerField()
 product_start_date = models.DateField()
 product_end_date = models.DateField()
 customer_message = models.TextField()

 def __str__(self):
  return self.customer_name
