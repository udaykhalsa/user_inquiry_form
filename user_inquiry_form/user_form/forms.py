from re import L
from django import forms
from .models import UserInquiry
from django.core.validators import RegexValidator

number_regex = RegexValidator(r'^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$', 'Please enter valid number')

class UserInquiryForm(forms.ModelForm):
 phone_number = forms.IntegerField(label='Phone Number', required=True, validators=[number_regex])
 class Meta:
  model = UserInquiry
  fields = ['customer_name', 'email_id', 'phone_number', 'product_start_date', 'product_end_date', 'customer_message']