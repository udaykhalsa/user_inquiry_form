from django.urls import path, include
from .views import *


urlpatterns = [
 path('', get_data_view, name='form_view'),
 path('otp-verification/', confirm_otp, name='confirm_otp'),
 # path('otp-verified', )
]
